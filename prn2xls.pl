#!/usr/bin/perl
##############################################################
#  Script     : prn2xls.pl
#  Author     : Pavels Resko
#  Date       : 16/10/2016
#  Last Edited: 20/10/2016, Pavels Resko
#  Description: script for converting *.prn files to *.xls
##############################################################
# Purpose:
# Requirements: Excel::Writer::XLSX;
#
# Method:
# Syntax: prn2xls.pl [input] [output]
# Example prn2xls.pl test/p_rMANGG559.prn output.xls
# Notes:
##############################################################

# modules

use strict;
use warnings;
use Excel::Writer::XLSX;
use Win32::GUI();

# programm constants
use constant DEBUG => 0;
use constant TEST => 0;
use constant WIN_MODE => 1;
use constant MIN_ARGC => 2;

# document constants
use constant FILE_HEADER_SIZE => 19;
use constant TABLE_HEADER_SIZE => 9;
use constant TABLE_FOOTER_SIZE => 3;
use constant PAGE_MIDDLE => 60;

# font constants
use constant FONT_SIZE => 10;
use constant HDR_FONT_SIZE => 9;
use constant FONT => "Arial";
use constant FONT_COLOR => "black";
use constant ROW_HEIGHT => 12.75;
use constant NO_HIDE_UNUSED => 0;
use constant HDR_ROW_HEIGHT => 63;

# column width constants
use constant COL_WIDHT_NR => 4;
use constant COL_WIDHT_DATE => 9.43;
use constant COL_WIDHT_NAME => 20;
use constant COL_WIDHT_NUM => 8.43;
use constant COL_WIDHT_CAR => 10.14;
use constant COL_WIDHT_KG => 9.86;
use constant COL_WIDHT_TYPE => 6.14;
use constant COL_WIDHT_AMT => 8.86;
use constant COL_WIDHT_21_VAT => 8.14;
use constant COL_WIDHT_0_VAT => 9.57;
use constant COL_WIDHT_NO_VAT => 8.14;
use constant COL_WIDHT_AMT_NO_VAT => 8.14;
use constant COL_WIDHT_VAT => 8.14;
use constant COL_WIDHT_SUM => 9;

# column format constants
use constant FMT_AMT => '0.00';
use constant FMT_NUM => '00000000';
use constant FMT_TYPE => '00';
use constant FMT_ID => '00';

# Table text constants
use constant TXT_SUM_FTR => "Kopsumma :";
use constant TXT_DOC => "Dokumenta";
use constant TXT_INCLUDING => "Tani skaita";
use constant TXT_ID => "Nr p/k";
use constant TXT_DATE => "Taksacijas \ndatums";
use constant TXT_NAME => "Nosaukums";
use constant TXT_NUM => "Numurs";
use constant TXT_CAR => "Vagona \n(konteinera) \nnumurs";
use constant TXT_KG => "Kravas svars,\n kg";
use constant TXT_TYPE => "Maksajuma \nveids";
use constant TXT_AMT => "Aprekinata\n summa, \nEUR";
use constant TXT_AMT_21_VAT => "Summa apliekama \nar 21% PVN, \nEUR";
use constant TXT_AMT_0_VAT => "Summa apliekama \nar 0% PVN, \nEUR";
use constant TXT_AMT_NO_VAT => "Summa neapliekama \nar PVN, \nEUR";
use constant TXT_AMT_NO_VAT2 => "Summa bez PVN,\n EUR";
use constant TXT_VAT => "PVN 21%,\n EUR";
use constant TXT_SUM => "Kopa,\n EUR";

# global variables
my @file_left_header = ();
my @file_right_header = ();
my @file_data = ();
my %special_summ;
my $unused;
my $signature;
my $inputfile;
my $outputfile;

#fuctions
sub isSepLine
{
	my $line = $_[0];
	my $result = $line =~ m/\x{c4}+/; 
	return $result;
}

sub isDocEnd
{
	my $line = $_[0];
	my $result = $line =~ m/Kopsumma/; 
	return $result;
} 

sub stripLine
{
	my $line = $_[0];
	$line =~ s/^\s+|\s+$//g;
	return $line;
}

sub fixChars
{
	my $line = $_[0];
	$line =~ tr/\x{d6}/g/;
	$line =~ tr/\x{f2}/G/;
	$line =~ tr/\x{f5}/l/;
	$line =~ tr/\x{de}/U/;
	$line =~ tr/\x{dd}/u/;
	$line =~ tr/\x{b7}/n/;
	$line =~ tr/\x{f0}/E/;
	$line =~ tr/\x{f1}/e/;
	$line =~ tr/\x{fd}/s/;
	$line =~ tr/\x{f4}/K/;
	$line =~ tr/\x{f3}/k/;
	$line =~ tr/\x{c6}/a/;
	$line =~ tr/\x{d8}/i/;
	$line =~ tr/\x{c4}/ /;
	$line =~ tr/\x{c2}/ /;
	$line =~ tr/\x{20}/ /;
	$line =~ tr/\x{0a}/ /;
	$line =~ tr/\x{0d}/ /;
	$line =~ tr/\x{0a}/ /;
	$line =~ tr/\x{b3}/ /;
	$line =~ tr/\x{bf}/ /;
	$line =~ tr/\x{da}/ /;
	return $line;
}

sub fixString
{
	my $line = $_[0];
	$line = fixChars( $line );
	$line= stripLine( $line );
	return $line;
}

sub showRecord
{
	my $data = $_[0];
	print "record---------------------------------------\n";
	while ( my($key, $value) = each %{$data} ) 
	{
		print "$key = $value\n";
	} 
}

sub getTableValues
{
	my $line = $_[0];

	# split by columns
	my @fields = split( /\x{b3}/, $line );

	# strip spaces
	foreach my $field (@fields)
	{
		$field = stripLine( $field );
	} 

	# convert to hash
	my %record;
	$record{nr} = $fields[1];
	$record{date} = $fields[2];
	$record{name} = fixChars( $fields[3] );
	$record{number} = $fields[4];
	$record{container} = $fields[5];
	$record{weight} = $fields[6];
	$record{payment_type} = $fields[7];
	$record{amt} = $fields[8];
	$record{amt_for_vat} = $fields[9];
	$record{amt_for_null_vat} = $fields[10];
	$record{amt_for_no_vat} = $fields[11];
	$record{amt_without_vat} = $fields[12];
	$record{vat} = $fields[13];
	$record{summ} = $fields[14];
	
	return %record;
}

sub getFooterValues
{
	my $line = $_[0];

	# split by columns
	my @fields = split( /\x{b3}/, $line );

	# strip spaces
	foreach my $field (@fields)
	{
		$field = stripLine( $field );
	} 

	# convert to hash
	my %record;
	$record{weight} = $fields[2];
	$record{amt} = $fields[3];
	$record{amt_for_vat} = $fields[4];
	$record{amt_for_null_vat} = $fields[5];
	$record{amt_for_no_vat} = $fields[6];
	$record{amt_without_vat} = $fields[7];
	$record{vat} = $fields[8];
	$record{summ} = $fields[9];
	
	return %record;
}

# main programm ---------------------------------------------------------------

# check that input and output is defined
if (TEST)
{
	$inputfile = "./test/p_rMANGG700.prn";
	$outputfile = "./output.xls";
}
elsif (@ARGV >= MIN_ARGC)
{
	$inputfile = $ARGV[0];
	$outputfile = $ARGV[1];
}
elsif (WIN_MODE)
{

    $inputfile = Win32::GUI::GetOpenFileName(-filemustexist => 1,);
    $outputfile = Win32::GUI::GetSaveFileName(-filemustexist => 0);  
}
else
{
    print "type in path for inputfile:\n";
    $inputfile = <>;
    print "type in path for outputfile:\n";
    $outputfile = <>;
}


if (length($inputfile) <= 1 or length($outputfile) <= 1)
{
	die("plese provide input and output files!\n");
}

# open input file
print "openning input file: $inputfile \n"; 
open( IN_FILE, $inputfile ) or die("Could not open $inputfile!");

# reading file header
print "reading file header...\n";
# dont need first two lines

# we will read register code separatelly
for(my $i = 0; $i < FILE_HEADER_SIZE-1; $i++)
{
	my $in_line = <IN_FILE>;
	my $left  = substr $in_line, 0, PAGE_MIDDLE;
	my $right = substr $in_line, - PAGE_MIDDLE;
	
	if ( length( $in_line ) < PAGE_MIDDLE ) 
	{
		$right = "";
	}

	$right = fixString( $right );
	$left = fixString( $left );
	if (DEBUG) 
	{
		print "left header: $left\n";
		print print "right header: $right\n";
	}
	push(@file_left_header,$left);
	push(@file_right_header,$right);
}

my $register_code = <IN_FILE>;
$register_code = fixString( $register_code );
if (DEBUG)
{
	print "register code: $register_code\n";
}

my $loop = 1;
while( $loop )
{
	# reading table header
	print "reading table header...\n";
	for(my $i = 0; $i < TABLE_HEADER_SIZE; $i++) 
	{
		my $line = <IN_FILE>;
		if (DEBUG)
		{
			print "table header: $line\n";
		}
	} 

	# reading table data
	print "reading table data...\n";
	while( 1 )
	{
		my $line = <IN_FILE>;
		if (DEBUG)
		{
			print "table data: $line\n";
		}
		if (isSepLine( $line ))
		{
			last;
		}  
		my %data = getTableValues( $line );
		push(@file_data,\%data);
	}

	# reading table footer
	print "reading table footer...\n";
	for(my $i = 0; $i < TABLE_FOOTER_SIZE; $i++) 
	{
		my $line = <IN_FILE>;  
		if (isDocEnd( $line ))
		{
			# found end of document
			$loop = 0;
			my %data = getFooterValues( $line );
			push(@file_data,\%data);
			$unused = <IN_FILE>;
			$unused = <IN_FILE>;
			$unused = <IN_FILE>;
			$signature = <IN_FILE>;
			$signature = fixString( $signature );
			print "end of the document...\n";
			last;
		}
	} 
}

close( IN_FILE );

# can check existing records
if (DEBUG)
{
	foreach my $data (@file_data)
	{
		showRecord( \%{$data} );
	} 
} 

# we need to add summ of code 01 and code 81, but only if both are present
# will be stored in special_summ
my %code_01;
my %code_81;
my $declaration_number;

foreach my $data (@file_data)
{
	if (${$data}{number})
	{
		$declaration_number = ${$data}{number};
	}

	my $code = ${$data}{payment_type};
	if ( $code )
	{
		if ( $code == 1 )
		{
			$code_01{$declaration_number} = ${$data}{summ};
		}
		elsif ( $code == 81 )
		{
			$code_81{$declaration_number} = ${$data}{summ};
		}
	}
}

foreach my $data (@file_data)
{
	my $declaration_number = ${$data}{number};
	if ($declaration_number)
	{
		my $c01 = $code_01{$declaration_number};
		my $c81 = $code_81{$declaration_number};
		if ($c01 and $c81 )
		{
			
			$special_summ{$declaration_number} = $c01 + $c81;
		}
	}
}

# can check existing records
if (DEBUG)
{
	foreach my $data (@file_data)
	{
		showRecord( \%{$data} );
	} 
} 

# generate output file
print "creating output file $outputfile... \n"; 

my $workbook  = Excel::Writer::XLSX->new( $outputfile ) or 
	die("could not create output file $outputfile!");
my $worksheet = $workbook->add_worksheet();

# formating for headers 
#$worksheet->set_default_row( ROW_HEIGHT, NO_HIDE_UNUSED );

my $fmt_file_hdr = $workbook->add_format( 
	border=>0, 
	color=> FONT_COLOR,
	align=> 'left',
	valign => 'vcenter',
	);
$fmt_file_hdr->set_font(FONT);
$fmt_file_hdr->set_size(FONT_SIZE);

my $fmt_hdr = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'center',
	valign => 'vcenter',
	);
$fmt_hdr->set_size(HDR_FONT_SIZE);
$fmt_hdr->set_text_wrap();
$fmt_hdr->set_font(FONT);
	
my $fmt_hdr_left = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'left',
	valign => 'vcenter',
	);
$fmt_hdr_left->set_size(HDR_FONT_SIZE);
$fmt_hdr_left->set_font(FONT);

my $fmt_special_summ = $workbook->add_format( 
	color=> FONT_COLOR,
	align=> 'right',
	valign => 'vcenter',
	);
$fmt_special_summ->set_size(FONT_SIZE);
$fmt_special_summ->set_font(FONT);

# formating for cells 
my $fmt_kg = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'right',
	valign => 'vcenter',
	);
$fmt_kg->set_size(FONT_SIZE);
$fmt_kg->set_font(FONT);

my $fmt_n = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'center',
	valign => 'vcenter',
	);
$fmt_n->set_size(FONT_SIZE);
$fmt_n->set_num_format(FMT_NUM);
$fmt_n->set_font(FONT);

my $fmt_container = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'center',
	valign => 'vcenter',
	);
$fmt_container->set_size(FONT_SIZE);
$fmt_container->set_font(FONT);

my $fmt_cell = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'left',
	valign => 'vcenter',
	);
$fmt_cell->set_size(FONT_SIZE);
$fmt_cell->set_font(FONT);

my $fmt_cell_id = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'center',
	valign => 'vcenter',
	);
$fmt_cell_id->set_size(FONT_SIZE);
$fmt_cell_id->set_num_format(FMT_ID);
$fmt_cell_id->set_font(FONT);
	
my $fmt_type = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'center',
	valign => 'vcenter',
	);
$fmt_type->set_size(FONT_SIZE);
$fmt_type->set_num_format(FMT_TYPE);
$fmt_type->set_font(FONT);
	
my $fmt_ammount = $workbook->add_format( 
	border=>1, 
	color=> FONT_COLOR,
	align=> 'right',
	valign => 'vcenter',
	);
$fmt_ammount->set_size(FONT_SIZE);
$fmt_ammount->set_num_format(FMT_AMT);
$fmt_ammount->set_font(FONT);

# row index
my $row_index = 1;

# generating file header
for(my $i = 2; $i < FILE_HEADER_SIZE-1; $i++) 
{
	$worksheet->write( "A$row_index", $file_left_header[$i], $fmt_file_hdr );
	$worksheet->write( "G$row_index", $file_right_header[$i], $fmt_file_hdr );
	$row_index++;
}

$row_index++;

# generating table header
$worksheet->merge_range( "A$row_index:C$row_index", $register_code, $fmt_hdr_left );
$row_index++;

$worksheet->merge_range( "B$row_index:D$row_index", TXT_DOC, $fmt_hdr );
$worksheet->merge_range( "I$row_index:L$row_index", TXT_INCLUDING, $fmt_hdr );
$worksheet->set_row( $row_index, HDR_ROW_HEIGHT );

my $prev_index = $row_index;
$row_index++;

my $column_index = 0;
$worksheet->merge_range( "A$prev_index:A$row_index", TXT_ID, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_NR );
$worksheet->write( "B$row_index", TXT_DATE, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_DATE );
$worksheet->write( "C$row_index", TXT_NAME, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_NAME );
$worksheet->write( "D$row_index", TXT_NUM, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_NUM );

$worksheet->merge_range( "E$prev_index:E$row_index", TXT_CAR, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_CAR );
$worksheet->merge_range( "F$prev_index:F$row_index", TXT_KG, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_KG );
$worksheet->merge_range( "G$prev_index:G$row_index", TXT_TYPE, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_TYPE );
$worksheet->merge_range( "H$prev_index:H$row_index", TXT_AMT, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_AMT );

$worksheet->write( "I$row_index", TXT_AMT_21_VAT, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_21_VAT );
$worksheet->write( "J$row_index", TXT_AMT_0_VAT, $fmt_hdr ); 
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_0_VAT );
$worksheet->write( "K$row_index", TXT_AMT_NO_VAT, $fmt_hdr );  
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_NO_VAT );
$worksheet->write( "L$row_index", TXT_AMT_NO_VAT2, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_AMT_NO_VAT );

$worksheet->merge_range( "M$prev_index:M$row_index", TXT_VAT, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_VAT );
$worksheet->merge_range( "N$prev_index:N$row_index", TXT_SUM, $fmt_hdr );
$worksheet->set_column( $column_index++ , 0,  COL_WIDHT_SUM );

$row_index++;

#generating data
foreach my $data (@file_data)
{
	$worksheet->write( "A$row_index", ${$data}{nr}, $fmt_cell_id );
	$worksheet->write( "B$row_index", ${$data}{date}, $fmt_cell );
	$worksheet->write( "C$row_index", ${$data}{name}, $fmt_cell );
	$worksheet->write( "D$row_index", ${$data}{number}, $fmt_n );
	$worksheet->write( "E$row_index", ${$data}{container}, $fmt_container );
	$worksheet->write( "F$row_index", ${$data}{weight}, $fmt_kg );
	$worksheet->write( "G$row_index", ${$data}{payment_type}, $fmt_type );
	$worksheet->write( "H$row_index", ${$data}{amt}, $fmt_ammount );
	$worksheet->write( "I$row_index", ${$data}{amt_for_vat}, $fmt_ammount );
	$worksheet->write( "J$row_index", ${$data}{amt_for_null_vat}, $fmt_ammount );
	$worksheet->write( "K$row_index", ${$data}{amt_for_no_vat}, $fmt_ammount );
	$worksheet->write( "L$row_index", ${$data}{amt_without_vat}, $fmt_ammount );
	$worksheet->write( "M$row_index", ${$data}{vat}, $fmt_ammount );
	$worksheet->write( "N$row_index", ${$data}{summ}, $fmt_ammount );
	
	my $declaration = ${$data}{number};
	if ($declaration)
	{
		$worksheet->write( "O$row_index", $special_summ{$declaration}, $fmt_special_summ);
	}
	
	$row_index++;
}

# Summ 
$row_index--;
$worksheet->write( "C$row_index", TXT_SUM_FTR , $fmt_cell );
$row_index++;
$row_index++;
$worksheet->write( "G$row_index", $signature , $fmt_file_hdr);

# we are done
$workbook->close;

print "output file $outputfile sucessfullly created\n"; 
exit 0;
