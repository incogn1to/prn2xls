Description: 
Script for converting printer (*.prn) files to MS Excel (*.xls).

Usage: 
prn2xls.pl [input] [output]

Example: 
prn2xls.pl test/p_rMANGG559.prn output.xls

Installation on Windows:
- Install ActivePerl for windows (for example http://www.activestate.com/activeperl/downloads/thank-you?dl=http://downloads.activestate.com/ActivePerl/releases/5.24.0.2400/ActivePerl-5.24.0.2400-MSWin32-x64-300558.exe )
- Launch Perl Package Manager (provided by ActivePerl)
- Search for Excel::Writer::XLSX
- Right click -> install Excel::Writer::XLSX
- Search for Win32::GUI 
- Right click -> isntall Win32::GUI
- Launch prn2xls.bat